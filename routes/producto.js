const express = require('express')
const router = express.Router()
const Producto = require('../controllers/Producto')
const auth = require ('../middleware/auth')

router.route('/')
.get(auth,Producto.getAll)
.post(auth,Producto.Create)
.put(Producto.NotSupported)
.delete(Producto.NotSupported)

router.route('/:productoId')
.get(auth,Producto.getOne)
.post(Producto.NotSupported)
.put(auth,Producto.Update)
.delete(auth,Producto.Delete);

module.exports = router