const express = require('express')
const router = express.Router()
const movimientos = require('../controllers/Movimiento')
const producto = require('../controllers/Producto')
const auth = require ('../middleware/auth')

router.route('/')
.get(auth,movimientos.getAll)
.post(auth,movimientos.Create,producto.ModificarCant,movimientos.Delete)

module.exports = router