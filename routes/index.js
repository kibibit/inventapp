const express = require('express')
const routes = express.Router()

const productRouter = require('./producto')
const storeRouter = require('./tienda')
const movementRouter = require('./movimiento')

routes.use('/producto',productRouter)
routes.use('/tienda',storeRouter)
routes.use('/movimiento',movementRouter)

module.exports = routes