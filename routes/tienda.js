const express = require('express')
const router = express.Router()
const Store = require('../controllers/Tienda')
const auth = require ('../middleware/auth')

//crear tienda
router.post('/register', Store.Register)

router.route('/')
.get(auth, Store.GetOne)
.post(Store.Login)

 module.exports = router