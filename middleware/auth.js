const jwt = require('jsonwebtoken')
//const Tienda = require('../models/Tienda')

const auth = async(req, res, next) => {
   try {
      const token = req.header('Authorization').replace('Bearer ', '')
      const tienda = await jwt.verify(token, process.env.JWT_KEY)
      //const user = await User.findOne({ _id: data._id, 'tokens.token': token })
      /*const tienda = await Tienda.findById(data._id)
      if (!tienda) {
         throw new Error()
      }*/
      req.userId = tienda._id
      //req.token = token
      next()
   } catch (error) {
      res.status(401).send({ error: 'Not authorized to access this resource' })
   }
}
module.exports = auth
