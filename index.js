const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');
const path = require('path');
const history = require('connect-history-api-fallback');

require('dotenv').config()
const port = process.env.PORT

mongoose.connect(process.env.DB_URL, { useNewUrlParser: true , useUnifiedTopology: true });
const db = mongoose.connection;
db.on('error', (error) => console.error(error));
db.once('open', () => console.log('connected to database'));
mongoose.set('useCreateIndex',true)

app.use(express.json());
app.use(cors());

const apiRouter = require('./routes');
app.use(apiRouter);

if (process.env.NODE_ENV === 'production') {
  app.use(history());
  app.use(express.static('client/dist'));
  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname,'client','dist','index.html'));
  });
}

// app.get('/', function (req, res) {
//     res.send('hello')
// });

app.listen(port, function () {
  console.log(`Server running on port ${port}`);
});