const Productos = require('../models/Producto')

const getAll = async(req,res,next) =>{
    try{
        productos = await Productos.find({'storeid':req.userId})
        if (productos.length === 0) {
            //No se encontraron productos
            return res.status(204).json()
        }
        res.status(200).json(productos)
    }catch(eer){
        return res.status(500).json({message: eer.message})
    }
    next()
}

const Create = async(req, res, next) => {  
    try{
        const producto = await Productos.create(req.body)
        res.status(201).json(producto)
    }catch(eer){
        return res.status(500).json({message: eer.message})
    }
    next()
}

const getOne = async(req,res,next) => {
    try{
        //const producto = Productos.findById(req.params.productoId)
        const producto = await Productos.findOne({ _id: req.params.productoId})
        if(producto == null){
            return res.status(404).send({error: 'No existe este producto'})
        }
        res.status(200).json(producto)
    }catch(eer){
        return res.status(500).json({message: eer.message})
    }
    next()
}

const Update = async(req, res, next) => {
    try{
        const producto = await Productos.findByIdAndUpdate(req.params.productoId, {
            $set: req.body
        }, { new: true })
        if(producto == null) return res.status(404).send({error: 'No existe este producto'})
        res.status(200).json(producto)
    }catch(eer){
        return res.status(500).json({message: eer.message})
    }
}

const Delete = async(req, res, next) => {
    try{
        const respuesta = await Productos.findByIdAndRemove(req.params.productoId)
        res.status(200).json(respuesta)
    }catch(eer){
        return res.status(500).json({message: eer.message})
    }
}

const NotSupported = (req, res, next) => {
    return res.status(405).json({message: 'Operation not supported'})
}

const ModificarCant = async(req, res, next) => {
    try{
        const prod = await Productos.findById(req.body.productid)
        if(prod == null) throw new Error('No existe este producto')
        if (req.body.buy) prod.quantity -= req.body.quantity
        else prod.quantity += req.body.quantity
        const producto = await Productos.findByIdAndUpdate(req.body.productid, {
            quantity : prod.quantity
        })
        if(producto == null) throw new Error('No se puede actualizar este producto, No existe')
        res.status(200).json(req.mov)
    }catch(eer){
        next()
    }
}

const prod = {getAll,Create,getOne,Update,Delete,NotSupported,ModificarCant}

module.exports = prod