const Movimientos = require('../models/Movimiento')

const getAll = async(req,res,next) => {
    try{
        const movs = await Movimientos.find({'storeid': req.userId})
        if(movs.length === 0) {
            //No se encontraron movimientos
            return res.status(204).json()
        }
        res.status(200).json(movs)
    }catch(eer){
        return res.status(500).json({error: eer.message})
    }
    next()
}

const Create = async(req,res,next) => {
    try{
        const movs = await Movimientos.create(req.body)
        req.mov = movs
    }catch(eer){
        return res.status(500).json({message: eer.message})
    }
    next()
}

const Delete = async(req, res, next) => {
    try{
        const respuesta = await Movimientos.findByIdAndRemove(req.mov._id)
        res.status(500).json('No se pudo crear el movimiento, Producto no encontrado')
    }catch(eer){
        return res.status(500).json({message: eer.message})
    }
}

const movimientos = {
    getAll,
    Create,
    Delete
}

module.exports = movimientos