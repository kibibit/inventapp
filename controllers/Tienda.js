const Tiendas = require('../models/Tienda')

const Register = async (req, res) => {
    try {
        const user = new Tiendas(req.body)
        await user.save()
        const disp = req.headers
        const token = await user.generateAuthToken(disp)
        res.status(201).send({ user, token })
    } catch (error) {
        res.status(400).send(error)
    }
}

const Login = async(req, res, next) => {
    try {
        const { email, password } = req.body
        const user = await Tiendas.findByCredentials(email, password)
        if (!user) {
            return res.status(401).json({error: 'Login failed! Check authentication credentials'})
        }
        const disp = req.headers
        const token = await user.generateAuthToken(disp)
        name=user.name
        id=user._id
        res.status(200).json({ id , name , token })
    } catch (error) {
        return res.status(400).json(error)
    }
    //next()
}

const GetOne = async(req, res) => {
    try{
        //const producto = Productos.findById(req.params.productoId)
        const tienda = await Tiendas.findOne({ _id: req.userId})
        if(tienda == null){
            return res.status(404).json({error: 'No existe esta tienda'})
        }
        res.status(200).json(tienda)
    }catch(eer){
        return res.status(500).json({message: eer.message})
    }
    //next()
 }

const tiendas = {Register,Login,GetOne}

module.exports = tiendas