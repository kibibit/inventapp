const mongoose = require('mongoose')

const movSchema = mongoose.Schema({
    buy: {
        type: Boolean,
        required: true
    },
    date: {
        type: Date,
        required: true,
        default: Date.now()
    },
    productid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Producto',
        required: true
    },
    quantity: {
        type: Number,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    total: {
        type: Number,
        required: true
    },
    storeid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Tienda',
        required: true
    }
})

module.exports = mongoose.model('Movimiento',movSchema)