const mongoose = require('mongoose')

const productSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    sellprice: {
        type: Number,
        required: true
    },
    buyprice: {
        type: Number,
        required: true
    },
    quantity: {
        type: Number,
        default: 0
    },
    category: {
        type: String
    },
    barcode: {
        type: String
    },
    storeid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Tienda'
    }
})

module.exports = mongoose.model('Producto',productSchema)